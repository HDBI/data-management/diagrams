# Diagrams

Created using [draw.io](https://www.drawio.com/). 

To ask questions or make suggestions please open an issue on this repo.
To submit contributions directly: fork this project, connect draw.io to your gitlab account, edit your fork of the diagram, save it to your fork with a commit message describing your edits and open a merge request from your fork.

## List of diagrams

 -   [Code Sharing & Publication](https://hdbi.gitlab.io/data-management/diagrams/code-sharing-and-publication/code-sharing-and-publication.drawio.html)
